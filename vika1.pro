#-------------------------------------------------
#
# Project created by QtCreator 2014-11-27T14:17:23
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = vika1
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    consoleui.cpp \
    person.cpp \
    personrepository.cpp \
    personservice.cpp

HEADERS += \
    consoleui.h \
    personservice.h \
    person.h \
    personrepository.h

OTHER_FILES += \
    welcome.txt
