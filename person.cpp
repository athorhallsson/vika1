#include "person.h"

Person::Person()
{
    name = "";
    birthYear = "";
    deathYear = "";
    gender = "";
}

Person::Person(const string& myName, const string& myBirthYear, const string& myDeathyear, const string& myGender)
{
    name = myName;
    birthYear = myBirthYear;
    deathYear = myDeathyear;
    gender = myGender;
}

ostream& operator << (ostream& out, const Person& p) {
    out << p.name << "\t" << p.birthYear << "\t" << p.deathYear << "\t" << p.gender << "\t";
    return out;
}

istream& operator >> (istream& in, Person& p) {
    in >> p.name >> p.birthYear >> p.deathYear >> p.gender;
    return in;
}

string Person::getName() const {
    return name;
}

string Person::getBirthyear() const {
    return birthYear;
}

string Person::getDeathyear() const {
    return deathYear;
}

string Person::getGender() const {
    return gender;
}
