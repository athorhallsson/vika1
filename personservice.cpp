
#include "personservice.h"

const char* LIST_PATH = "../list.txt";      // constant for the list.txt path

PersonService::PersonService() {
    personRepo = PersonRepository();
}

void PersonService::add(Person p) {
    personRepo.add(p);
    save();
}

bool PersonService::save() {
    ofstream list (LIST_PATH);
    if (list.is_open()) {
        for (unsigned int i = 0; i < personRepo.getSize(); i++) {
            list << personRepo.getPersonAtIndex(i);
        }
        list.close();
        return true;
    }
    return false;
}

bool PersonService::load() {
    string name, birthYear, deathYear, gender;
    ifstream list (LIST_PATH);
    if (list.is_open()) {
        while (getline(list, name, '\t')) {
            getline(list, birthYear, '\t');
            getline(list, deathYear, '\t');
            getline(list, gender, '\t');
            Person p(name, birthYear, deathYear, gender);
            personRepo.add(p);
        }
        list.close();
        return true;
    }
    return false;
}

vector<int> PersonService::find(const string& searchName) {
    vector<int> locationVector(0);                                          // create a new vector
    for (unsigned int i = 0; i < personRepo.getSize(); i++) {
        unsigned int pos;
        pos = personRepo.getPersonAtIndex(i).getName().find(searchName);    // find function from string library
        if (pos < personRepo.getPersonAtIndex(i).getName().size()) {       // find returns pos > size() if not found
            locationVector.push_back(i);
        }
    }
    return locationVector;
}

void PersonService::clear() {
    personRepo.clear();
}

void PersonService::remove(int index) {
   personRepo.remove(index);
}

Person PersonService::getPersonAtIndex(int index) {
    return personRepo.getPersonAtIndex(index);
}

bool PersonService::sort(const string& sortingMethod) {
    if (sortingMethod == "1") {
        personRepo.sortAlphabetically();
        return true;
    }
    else if (sortingMethod == "2") {
        personRepo.sortReverseAlphabetically();
        return true;
    }
    else if (sortingMethod == "3") {
        personRepo.sortByBirthyear();
        return true;
    }
    else if (sortingMethod == "4") {
        personRepo.sortByReverseBirthyear();
        return true;
    }
    else if (sortingMethod == "5") {
        personRepo.sortByGender();
        return true;
    }
    else if (sortingMethod == "6") {
        personRepo.sortByReverseGender();
        return true;
    }
    return false;
}

unsigned int PersonService::getSize() {
    return personRepo.getSize();
}
