#ifndef PERSONREPOSITORY_H
#define PERSONREPOSITORY_H

#include <vector>
#include "person.h"
#include <fstream>
#include <algorithm>

class PersonRepository
{
public:
    PersonRepository();
    Person getPersonAtIndex(int index);
    void add(const Person& p);
    void clear();
    void remove(int index);
    void sortAlphabetically();
    void sortReverseAlphabetically();
    void sortByBirthyear();
    void sortByReverseBirthyear();
    void sortByGender();
    void sortByReverseGender();
    unsigned int getSize();
private:
    vector<Person> personVector;
};

#endif // PERSONREPOSITORY_H
