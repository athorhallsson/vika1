#ifndef PERSONSERVICE_H
#define PERSONSERVICE_H

#include "personrepository.h"

class PersonService {
public:
    PersonService();
    void add(Person p);
    Person getPersonAtIndex(int index);
    bool save();
    bool load();
    void clear();
    vector<int> find(const string &searchName);
    void remove(int index);
    bool sort(const string& sortingMethod);
    unsigned int getSize();
private:
    PersonRepository personRepo;

};

#endif // PERSONSERVICE_H
