#ifndef CONSOLEUI_H
#define CONSOLEUI_H

#include "personservice.h"

class consoleUi
{
public:
    consoleUi();
    void start();
private:
    PersonService personService;
    void printActions();
    void execute(char action);
    void newline();
    void sort();
    void find();
    void remove();
    Person fillInfo();
    void displayInfo(const Person &p);
    void display();
    void printWelcome();
};


#endif // CONSOLEUI_H
