#include "consoleui.h"

consoleUi::consoleUi()
{
    personService = PersonService();
    if(!personService.load()) {
        cout << "Error. Unable to load." << endl;
    }
    printWelcome();
}

void consoleUi::start() {
    char action;
    do {
        cout << "Select action" << endl;
        printActions();
        cin >> action;
        execute(action);
    } while (action != 'q');
}

void consoleUi::printActions() {
    cout << "a: add" << endl;
    cout << "d: display" << endl;
    cout << "c: clear" << endl;
    cout << "f: find" << endl;
    cout << "k: save" << endl;
    cout << "r: remove" << endl;
    cout << "s: sort" << endl;
    cout << "q: quit" << endl;
}

void consoleUi::execute(char action) {
    Person p;
    switch (action) {
    case 'a':
        p = fillInfo();
        personService.add(p);
        break;
    case 'c':
        personService.clear();
        break;
    case 'd':
        display();
        break;
    case 'f':
        find();
        break;
    case 'q':
        personService.save();
        break;
    case 'r':
        remove();
        break;
    case 's':
        sort();
        break;
    case 'k':
        if(!personService.save()) {
            cout << "Error. Unable to save." << endl;
        }
        break;
    default:
        cout << "That is not a valid command, please pick again." << endl;
    }
}

void consoleUi::newline() {
    char input;
    do
    {
        cin.get(input);
    } while (input != '\n');              // to clear the stream
}

void consoleUi::sort() {
    string sortingMethod;
    cout << endl;
    cout << "Select a sorting method: " << endl;
    cout << "1: Alphabetical order" << endl;
    cout << "2: Reverse alphabetical order" << endl;
    cout << "3: Oldest-youngest" << endl;
    cout << "4: Youngest-oldest" << endl;
    cout << "5: Women-men" << endl;
    cout << "6: Men-women" << endl;
    cin >> sortingMethod;
    if (personService.sort(sortingMethod)) {
        display();
    }
    else {
        cout << "Not a valid sorting method." << endl;
    }

}

void consoleUi::find() {
    vector<int> locationVector(0);
    string searchName;
    cout << endl << "Enter name to search for: ";
    newline();
    getline(cin,searchName);
    locationVector = personService.find(searchName);
    if (locationVector.size() == 0) {
        cout << "Person not found." << endl << endl;
    }
    else {
        cout << "The list contains " << locationVector.size() << " instances of " << searchName << "." << endl;
        for (unsigned int i = 0; i < locationVector.size(); i++)  {
            displayInfo(personService.getPersonAtIndex(locationVector[i]));
        }
    }
}

void consoleUi::remove() {
    string index;
    int indexInt;
    string symbol;
    string searchName;
    cout << endl << "Enter the name of the person you would like to remove: ";
    newline();
    getline(cin,searchName);
    vector<int> locationVector = personService.find(searchName);
    if (locationVector.size() == 0) {
        cout << "Person not found." << endl << endl;
    }
    else if (locationVector.size() == 1) {
            cout << "The list contains 1 instance of " << searchName << "." << endl;
            displayInfo(personService.getPersonAtIndex(locationVector[0]));
            cout << "Do you want to remove the person? (y/n) ";
            cin >> symbol;
            if (symbol == "y" || symbol == "Y") {
                personService.remove(locationVector[0]);
            }
            else {
                cout << "Action canceled." << endl;
            }
    }
    else {
        cout << "The list contains " << locationVector.size() << " instances of " << searchName << "." << endl;
        for (unsigned int i = 0; i < locationVector.size(); i++)  {
            cout << i+1 << ": ";
            displayInfo(personService.getPersonAtIndex(locationVector[i]));
        }
        cout << "Select the person you want to remove (1-" << locationVector.size() << "):";

        cin >> index;
        indexInt = atoi(index.c_str());
        indexInt--;
        while (!(indexInt >= 0 && indexInt < static_cast<int>(locationVector.size()))) {
            cout << "Not valid. Try again." << endl;
            cin >> index;
            indexInt = atoi(index.c_str());
            indexInt--;
        }
        personService.remove(locationVector[indexInt]);
    }
}

Person consoleUi::fillInfo() {
    string name, birthYear, deathYear, gender;
    cout << endl;
    newline();
    cout << "Name: ";
    getline(cin,name);
    cout << "Born: ";
    cin >> birthYear;
    cout << "Died: ";
    cin >> deathYear;
    cout << "Gender (m/f): ";
    cin >> gender;
    cout << endl;
    return Person(name, birthYear, deathYear, gender);
}

void consoleUi::displayInfo(const Person& p) {
    cout << endl << p.getName() << endl << p.getBirthyear() << "-" << p.getDeathyear() << endl;
    string gender = p.getGender();
    if (gender == "m") {
        cout << "male";
    }
    else if (gender == "f") {
        cout << "female";
    }
    else {
        cout << "n/a";
    }
    cout << endl << endl;
}

void consoleUi::display() {
    for(unsigned int i = 0; i < personService.getSize(); i++) {
        displayInfo(personService.getPersonAtIndex(i));
    }
}

void consoleUi::printWelcome() {
    ifstream welcome ("../welcome.txt");
    if (welcome.is_open()) {
        cout << welcome.rdbuf();
        welcome.close();
    }
    else {
        cout << "Error. Unable to print welcome." << endl;
    }

}
